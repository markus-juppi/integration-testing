const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3000;

describe("Color code converter API", () => {
    before("starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to HEX conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0"
        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("return the color in HEX", (done) => {
            request(url + "", (error, response, body) => {
                expect(body).to.equal("#ff0000");
                done();
            });
        });
    });
    describe("HEX to RGB conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        const url = baseurl + "/hex-to-rgb?hex=ff0000"
        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("return the color in RGB", (done) => {
            request(url + "", (error, response, body) => {
                expect(body).to.equal("255,0,0");
                done();
            });
        });
    });
    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});