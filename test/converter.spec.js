// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to HEX conversions", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            expect(redHex).to.equal("#ff0000"); // red hex value
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value
        });
    });
    describe("HEX to RGB conversions", () => {
        it("converts the basic colors", () => {
            const redRGB = converter.hexToRgb("ff0000");
            expect(redRGB).to.equal("255,0,0");
            const blueRGB = converter.hexToRgb("0000ff");
            expect(blueRGB).to.equal("0,0,255");
        })
    })
});