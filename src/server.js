const express = require('express');
const converter = require('./converter');
const app = express();
const PORT = 3000;

// Welcome
app.get('/', (req, res) => res.send("Welcome!"));

//RGB to Hex output
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.r);
    const green = parseInt(req.query.g);
    const blue = parseInt(req.query.b);
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
});
//Hex to RGB output
app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;
    const rgb = converter.hexToRgb(hex);
    res.send(rgb);
});
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => {
        console.log("Server listening...")
    });
}

console.log("NODE-ENV: "+ process.env.NODE_ENV)