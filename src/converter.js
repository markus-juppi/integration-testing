/**
 * Padding output to match 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /**
     * Converts the RGB values to a Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} Hex value
     */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },

    /**
     * Converts the HEX sting to RGB values
     * @param {string} hex xxxxxx
     * @returns {number, number, number} 0-255, 0-255, 0-255
     */
    hexToRgb: (hex) => {
        var validHEXInput = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        if (!validHEXInput) {return false;}
        r = parseInt(validHEXInput[1], 16);
        g = parseInt(validHEXInput[2], 16);
        b = parseInt(validHEXInput[3], 16);
        return r+","+g+","+b;
    }
};